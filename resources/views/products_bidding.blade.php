@extends('layouts.app')
@section('content')

<div class="container">
	<div class="row">
		
			@if(! empty($messageType) && $messageType=='success')
			<div class="alert alert-success">
  				<strong>Successfully Submitted!</strong> {{$count}} Products were added.
			</div>
			@endif

			@if(! empty($messageType) && $messageType=='error')

			<div class="alert alert-warning">
  				<strong>Warning!</strong> Empty Feilds will not be submitted.
  				
			@if( ! empty($message))
			    <label name="lblVenName">{{$message}}</label>
			@endif

			</div>
			@endif

<!-- 
			@if( ! empty($message))
			    <label name="lblVenName">{{$message}}</label>
			@endif -->
</div>
</div>

<div class="container">
<div class="row">
<form action="/addbidding" method="POST"  class="form-inline">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" name="products_length" value="{{count($products)}}">

	
	<label name="lblVenName">Vendor Name :</label>
	<input class="form-control mb-2 mr-sm-2 mb-sm-0" type="text" name="txtVendorName">

	<div class="container">
<table class="table table-hover" cellpadding="100px">
<thead>
<tr>
	<th scope="col">Product Name</th>
	<th scope="col">Vendor Price ($)</th>
	<th scope="col">Units</th>
	<th scope="col">Alternate Product</th>
	<th scope="col">Alternate Price ($)</th>
	<th scope="col">Units</th>
</tr>
	</thead>
	<tbody>
@foreach ($products as $key=>$product) 
<tr>
	<td>
		<input type="hidden" name="product_id{{$key}}" value="{{ $product->id }}">
		<label name="lblProName">{{$product->product_name}}</label>
	</td>

	<td>
		<input class="form-control mb-2 mr-sm-2 mb-sm-0" type="number" name="txtProdPrice{{$key}}" min="0" >

	</td>

	<td>
		<label name="lblProName">{{$product->unit}}</label>
	</td>

	<td>
		<input class="form-control mb-2 mr-sm-2 mb-sm-0" type="text" name="txt_alt_product{{$key}}">
	</td>
	<td>
		<input class="form-control mb-2 mr-sm-2 mb-sm-0" type="number" name="txt_alt_price{{$key}}" min="0">
	</td>
	<td>
		<input class="form-control mb-2 mr-sm-2 mb-sm-0" type="text" name="txt_units{{$key}}">
	</td>

</tr>
@endforeach

<tr>
<div class="form-check-inline">
	<td><center><button type="submit" name="submit" class="btn btn-primary" >Submit</button></center></td>

</div>
</tr>
</tbody>
</table>
</form>
</div>
</div>
@endsection