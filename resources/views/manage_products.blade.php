@extends('layouts.app')
@section('content')
@if(empty($update_mode))
<div class="container">
	<div class="row">
		
		@if(! empty($messageType) && $messageType=='success')
			<div class="alert alert-success">
  			<strong>Successfully Submitted!</strong>
			</div>
			@endif

			@if(! empty($messageType) && $messageType=='error')

			<div class="alert alert-warning">
  			<strong>Warning!</strong> Empty Feilds will not be submitted.
  			
			@if( ! empty($message))
			<label name="lblVenName">{{$message}}</label>
			@endif

			</div>
			@endif
		
			<form class="form-inline" action="/product/add" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

			  <!--<label class="sr-only" for="inlineFormInput">Name</label>-->
			  <input type="text" name="product" class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput" placeholder="Enter product name">

			 <!-- <label class="sr-only" for="inlineFormInputGroup">Username</label>-->
			  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
			    <input type="text" name="unit"class="form-control" id="inlineFormInputGroup" placeholder="Enter unit">
			  </div>

			  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
			  <button type="submit" class="btn btn-primary">Submit </button>
			    
			  </div>

			</form>
</div>
</div>
@endif
@if(! empty($update_mode) && $update_mode===true)
<div class="container">
			<div class="row">
				<form class="form-inline" action="/product/update/{{ $product->id }}" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					 <!-- <label class="sr-only" for="inlineFormInput">Name</label>-->
					  <input type="text" name="product" class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput" placeholder="Enter product name" value="{{$product->product_name}}" style="color:red;">

					 <!-- <label class="sr-only" for="inlineFormInputGroup">Username</label> {{$product->unit}}    -->
					  <div class="input-group mb-2 mr-sm-2 mb-sm-0">

					    <input type="text" value="{{$product->unit}}" name="unit" class="form-control" id="inlineFormInputGroup" placeholder="Enter unit" style="color:red;">
					  </div>

					  <!--<div class="form-check-inline">-->
					  <div class="input-group mb-2 mr-sm-2 mb-sm-0">
					  <button type="submit"  class="btn btn-primary">Update </button>
					    
					  </div>
					</form>
		</div>
		</div>
@endif

<!--Code for edit ends here-->

<div class="container">
<table class="table table-hover" cellpadding="100px">
  <thead>
    <tr>
      <th scope="col">Product Id</th>
      <th scope="col">Product Name</th>
      <th scope="col">Unit</th>
      <th scope="col">Edit</th>
      <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody>
  @foreach ($products as $key=>$product) 
	    <tr>
	      <td>{{$product->id}}</td>
	      <td>{{$product->product_name}}</td>
	      <td>{{$product->unit}}</td>
	     <td>
	     	<a href="/product/edit/{{$product->id}}" class="btn btn-primary a-btn-slide-text">
	     	<!--<button  class="btn btn-primary a-btn-slide-text">-->
	     	<button class="btn btn-primary a-btn-slide-text">
	     	
	        <span><strong>Edit</strong></span>
	        </button> 
	        </a>          
	    </td>
	     <td>

<!--real code starts-->
<!--
	    <a href="/product/delete/{{$product->id}}" onclick="return confirm('Are you sure?')" class="btn btn-primary a-btn-slide-text">
        <span><strong>Delete</strong></span>            
    	</a> 
-->
<!--real code ends-->
		<a href="/product/delete/{{$product->id}}" onclick="return confirm('Are you sure?')" class="btn btn-primary a-btn-slide-text">
		<button  class="btn btn-primary a-btn-slide-text">
        <span><strong>Delete</strong></span>
        </button>            
    	</a>
	    </td>
	    </tr>
	@endforeach
</tbody>
</table>
</div>
@endsection