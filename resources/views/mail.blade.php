<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>

    </head>
    <body>
<?php
$products = json_decode($data, true);
?>
<!--<table class="table table-hover" cellpadding="100px">-->
<table border="1">
<thead>
<tr>
	<th scope="col">Product Name</th>
	<th scope="col">Vendor Price</th>
	<th scope="col">Vendor Name</th>
	<th scope="col">Alternate Product</th>
	<th scope="col">Alternate Price</th>
	<th scope="col">Alternate Unit</th>
	<th scope="col">Alternate Vendor Name</th>
</tr>
	</thead>
	<tbody>
	<?php
foreach($products as $product)
{
echo '
	    <tr>';
	    $counter = 0;

	    foreach($product as $p)
	    {
	    	$counter++;
	    	echo '<td>'.$p.'</td>';
	    	
	    }

	    for($i = $counter ; $i < 7 ; $i++)
	   {

	   	echo '<td> </td>';
	   	
	   }
	   echo ''.'            
	    	    </tr>
	';

}
?>
</tbody>
</table>
</body>
</html>