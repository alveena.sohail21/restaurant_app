<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use \App\product;
class manageProducts extends Controller
{
    public function index()
    {
        $products = \App\product::all();
        $update_mode=false;
        return view('manage_products',compact('products',$products,'update_mode',$update_mode));
    }

 	public function delete($id)
    {
    	\App\product::destroy((int)$id);
        $products = \App\product::all();
        $update_mode=false;
        return view('manage_products',compact('products',$products,'update_mode',$update_mode));
    }
    public function add(Request $req)
    {
        $messageType='error';
        $message='';
        
    	$product = $req->input('product');
    	$unit = $req->input('unit');

        if(!is_null($product) && !is_null($unit))
        {
        $messageType='success';
        $product_obj=new product;
    	$product_obj->product_name=$product;
    	$product_obj->unit=$unit;
    	$product_obj->save();

        }

        $products = product::all();
        $update_mode=false;
        return view('manage_products',compact('products',$products,'message',$message,'messageType',$messageType,'update_mode',$update_mode));
    }
    public function edit($id)
	    {
	    	$product=product::find((int)$id);
	    	$products = product::all();
	    	$update_mode=true;
	        return view('manage_products',compact('product',$product,'update_mode',$update_mode,'products',$products));  
    }
    public function update(Request $req,$id)
    {
    	$product = $req->input('product');
    	$unit = $req->input('unit');

    	$productObj=product::find((int)$id);
    	//$product=$req->input('product');
    	$productObj->product_name=$product;
    	$productObj->unit=$unit;

    	$productObj->save();
    	$update_mode=false;
    	$products=product::all();
    	return view('manage_products',compact('product',$product,'products',$products , 'update_mode',$update_mode));

    }
}