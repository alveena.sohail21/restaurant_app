<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\product;
use App\unit;
use App\bidding;

class biddingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = \App\product::all();
        return view('');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products=product::all();
        $units=unit::all();
        return view('products_bidding',compact('products',$products,'units',$units));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        
        $messageType='error';
        $message='';
        $count=0;

        
        $product_length=$req->input('products_length');
        for ($i=0; $i <$product_length ; $i++) { 
          # code...
       
        $vendor_name = $req->input('txtVendorName');
       
        $pro_id = $req->input('product_id'.$i);
        $product_price = $req->input('txtProdPrice'.$i);
        
        $alt_product=$req->input('txt_alt_product'.$i);
        $alt_vend_price = $req->input('txt_alt_price'.$i);
        $alt_units = $req->input('txt_units'.$i);
            if(!is_null($vendor_name) && !is_null($product_price) ||!is_null($vendor_name) && !is_null($alt_product) &&  !is_null( $alt_vend_price) && !is_null($alt_units) ){
                
                $count=$count+1;
                $messageType='success';
                $bidding=new bidding;
                $bidding->product_id=(int)$pro_id;
                $bidding->vendor_name=$vendor_name;
                $bidding->alternate_product=$alt_product;
                $bidding->vendor_price=(int)$product_price;
                $bidding->alternate_units= $alt_units;
                $bidding->alt_vendor_price=(int)$alt_vend_price;
                $bidding->save();

            }
      }

      if(is_null($vendor_name))
      {
        $message='Vendor name is required';
      }
        
        $products=product::all();
        $units=unit::all();
        return view('products_bidding',compact('products',$products,'units',$units,'message',$message,'messageType',$messageType,
        'count',$count));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}