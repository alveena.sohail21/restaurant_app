<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Mail;
use Config;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\product;
use Carbon;

class MailController extends Controller
{
    public function html_email(){
    //dd(Config::get('mail'));

        //My work starts

        $products=product::all();
        $rows=array();
        foreach($products as $p)
        {
        $date = new Carbon\Carbon; //  DateTime string will be 2014-04-03 13:57:34

        $date->subWeek(); // or $date->subDays(7),  2014-03-27 13:58:25

        $temp=$p->biddings()->where('created_at','>',$date->toDateTimeString())->orderby('vendor_price')->first();
        $temp2=$p->biddings()->where('created_at','>',$date->toDateTimeString())->orderby('alt_vendor_price')->whereNotNull('alternate_product')->first();
        if($temp){
        //$row=array('product_name' => $p->product_name,'vendor_price' =>$temp->vendor_price,'vendor_name' =>$temp->vendor_name);    
                $row=array( $p->product_name,$temp->vendor_price,$temp->vendor_name);    
        }
        if($temp2){


            // $row2=array('alternate_product' =>$temp2->alternate_product,
            //     'alternate_vendor_price' =>$temp2->alt_vendor_price,
            //     'alternate_units' =>$temp2->alternate_units,'alternate_vendor_name' =>$temp2->vendor_name);

            
            array_push($row,$temp2->alternate_product,
                $temp2->alt_vendor_price,
                $temp2->alternate_units,$temp2->vendor_name);
        }
        
        array_push($rows,$row);
        }

        //ends here


      $data = json_encode($rows,JSON_FORCE_OBJECT);
     // return view('mail',compact('data',$data));
      Mail::send('mail', compact('data',$data), function($message) {
         $message->to('kkentros@gmail.com', 'KKentros')->subject
            ('Daily Product Bidding');
         $message->from('Restaurant@gmail.com','Manager');
      });
      echo "HTML Email Sent. Check your inbox.";
   }



    //Pasted code starts here
   /* public function basic_email()
    {
      $data = array('name'=>"Virat Gandhi");
   
      Mail::send(['text'=>'mail'], $data, function($message) {
         $message->to('abc@gmail.com', 'Tutorials Point')->subject
            ('Laravel Basic Testing Mail');
         $message->from('xyz@gmail.com','Virat Gandhi');
      });
      echo "Basic Email Sent. Check your inbox.";
   }*/

   /*public function attachment_email()
   {
      $data = array('name'=>"Virat Gandhi");
      Mail::send('mail', $data, function($message) {
         $message->to('abc@gmail.com', 'Tutorials Point')->subject
            ('Laravel Testing Mail with Attachment');
         $message->attach('C:\laravel-master\laravel\public\uploads\image.png');
         $message->attach('C:\laravel-master\laravel\public\uploads\test.txt');
         $message->from('xyz@gmail.com','Virat Gandhi');
      });
      echo "Email Sent with attachment. Check your inbox.";
   }*/


    // pasted code ends here

/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}