<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

;

Route::get('/','biddingController@create');

Route::post('/addbidding','biddingController@store');
//Controller for update
Route::post('/product/update/{id}','manageProducts@update');

Route::get('/manage_products','manageProducts@index');
Route::get('/product/delete/{id}','manageProducts@delete');
Route::post('/product/add','manageProducts@add');
Route::get('/product/edit/{id}','manageProducts@edit');


Route::get('sendemail','MailController@html_email');

Route::resource('biddings','biddingController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::post('/insert' , 'Controller@insert');
Route::get('Controller' , array());
